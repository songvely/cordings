#include <iostream>

using namespace std;

class Oval{
  int width, height;
    public:
    Oval(){}
    Oval(int x, int y)
        :width(x), height(y){}
    ~Oval(){
        cout << "Oval 소멸 : " << "width = " << width << ", height = " << height << endl;
    }
    int getWidth(){
        return width;
    }
    int getHeight(){
        return height;
    }
    void set(int w, int h){
        width=w, height=h;
    }
    void show(){
        cout << "width = "<< width << ", height = " << height << endl;
    }
};

int main(){
    Oval a(10,20), b;
    a.show();
    b.set(3,4);
    cout << b.getWidth() << ", " << b.getHeight() << endl;
}