#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
using namespace std;

class Person {
	string man[2];
	int i;
public:
	Person() { man[0] = "김인수", man[1] = "오은경", i = -1; }
	string getName() {
		i++;
		if (i == 2) {
			i = 0;
			return man[i];
		}
		else return man[i];
	}
};

class UpAndDownGame {
	static int low, high;
public:
	void run();
};

void UpAndDownGame::run() {
	Person p;
	srand((unsigned)time(0));
	int n = rand();
	n = n % 100;

	cout << "Up & Down 게임을 시작합니다." << endl;
	cout << "답은 " << low << "과 " << high << "사이에 있습니다." << endl;
	int num;
	while (1) {
		string name = p.getName();
		cout << name << ">>";
		cin >> num;
		if (num == n) {
			cout << name << "가 이겼습니다!!";
			break;
		}
		if (num > n) high = num;
		else low = num;
		cout << "답은 " << low << "과 " << high << "사이에 있습니다." << endl;
	}
}
int UpAndDownGame::low = 0;
int UpAndDownGame::high = 99;

int main() {
	UpAndDownGame g;
	g.run();
}