#include <iostream>
using namespace std;

class Container {
	int size;
public:
	Container() { size = 10; }
	void fill() { size = 10; }
	void consume(int n) { size -= n; }
	int getSize() { return size; }
};

class CoffeeVendingMachine {
	Container tong[3];
	void fill();
	void selectEspresso() { tong[0].consume(1), tong[1].consume(1); }
	void selectAmericano() { tong[0].consume(1), tong[1].consume(2); }
	void selectSugarCoffee() { tong[0].consume(1), tong[1].consume(2), tong[2].consume(1); }
	void show() {
		cout << "커피 " << tong[0].getSize() << ", 물 " << tong[1].getSize() << ", 설탕 " << tong[2].getSize() << endl;
	}

public:
	void run();
};
void CoffeeVendingMachine::fill() {
	for (int i = 0; i < 3; i++)
		tong[i].fill();
	show();
}
void CoffeeVendingMachine::run() {
	int num;
	int tmp = 1;
	fill();
	while (true) {
		cout << "메뉴를 눌러주세요(1:에스프레소, 2:아메리카노, 3:설탕커피, 4:잔량보기, 5:채우기)>>";
		cin >> num;
		switch (num) {
		case 1: {
			selectEspresso();
			tmp = 1;
			for (int i = 0; i < 3; i++) {
				if (tong[i].getSize() < 0) {
					tmp = 0;
					cout << "원료가 부족합니다." << endl;
					break;
				}
			}
			if (tmp == 0)
				continue;

			cout << "에스프레소 드세요" << endl;
			break;
		}
		case 2:
		{
			selectAmericano();
			tmp = 1;
			for (int i = 0; i < 3; i++) {
				if (tong[i].getSize() < 0) {
					tmp = 0;
					cout << "원료가 부족합니다." << endl;
					break;
				}
			}
			if (tmp == 0)
				continue;

			cout << "아메리카노 드세요" << endl;
			break;
		}
		case 3:
		{
			selectSugarCoffee();
			tmp = 1;
			for (int i = 0; i < 3; i++) {
				if (tong[i].getSize() < 0) {
					tmp = 0;
					cout << "원료가 부족합니다." << endl;
					break;
				}
			}
			if (tmp == 0)
				continue;
			cout << "설탕커피 드세요" << endl;
			break;
		}
		case 4:
			show();
			break;
		case 5:
			fill();
			break;
		}
	}
}
int main() {
	CoffeeVendingMachine c;
	cout << "***** 커피자판기를 작동합니다. *****" << endl;
	c.run();
}