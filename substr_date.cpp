#include <iostream>
#include <string>
using namespace std;

class Date {
	int year, month, day;
public:
	Date(int a, int b, int c)
		: year(a), month(b), day(c) {}
	Date(string s);
	int getYear() {
		return year;
	}
	int getMonth() {
		return month;
	}
	int getDay() {
		return day;
	}
	void show() {
		cout << year << "년" << month << "월" << day << "일" << endl;
	}
};

Date::Date(string s) {
	int a, b, c;
	for (int i = 0; i<s.length(); i++) {
		if (i == 4) {
			a = stoi(s.substr(0, i));
			year = a;
		}
		else if (i == 6) {
			b = stoi(s.substr(5, i));
			month = b;
		}
		else if (i == 8) {
			c = stoi(s.substr(7, i));
			day = c;
		}
	}
}
int main() {
	Date birth(2014, 3, 20);
	Date independenceDay("1945/8/15");
	independenceDay.show();
	cout << birth.getYear() << ',' << birth.getMonth() << ',' << birth.getDay() << endl;
}