#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>

int p, i, j = 0, a, b; //p=인원수, i,j=반복문에 쓰일 변수, a,b=빙고판 출력에 쓰일 변수
char name1[50], name2[50], name3[50]; //user 이름을 저장할 배열
void print_bingo(char arr[25], char arr2[25], char arr3[25]); //빙고판 출력 함수

int main(void)
{
	char r, r2, r3;  //난수를 저장할 변수
	char select1, select2, select3; //user가 입력한 알파벳 select1은 user1, select2는 user2..
	int k, l = 0, m, n = 0, x; //(k,l)-user2 난수 생성 반복문에 쓰이는 변수, (m,n)-user3, x는 user가 선택한 알파벳이 배열에서 어느 곳에 위치하고 있는지 검사하는 변수
	char* user1 = (char*)malloc(sizeof(char) * 25); //user1의 난수를 담을 배열
	char* user2 = (char*)malloc(sizeof(char) * 25); //user2의 난수를 담을 배열
	char* user3 = (char*)malloc(sizeof(char) * 25); //user3의 난수를 담을 배열

	srand((int)time(NULL)); //컴파일마다 숫자가 달라질 수 있도록 시드 설정

	printf("인원수를 입력하세요.(2 or 3)>> ");
	scanf("%d", &p); //인원수 입력

	if (p == 2) { //입력한 숫자가 2일 때

		printf("User 1의 이름을 입력하세요.>> ");
		scanf("%s", name1);
		printf("User 2의 이름을 입력하세요.>>");
		scanf("%s", name2);
		system("cls"); //화면 초기화

		for (i = 0; i < 25; i++) { //user1 난수 만들기
			while (j<25)
			{
				r = rand() % 25 + 'A'; //알파벳 랜덤

				for (j = 0; j <= i; j++)  //r값이 지정된 상태에서, 앞에서 중복된 알파벳이 있는지 검사
					if (r == user1[j])
						break;  //만약 중복된게 있을시 반복 중지
				if (j < i)
					continue;  //j와 i가 같아질 때까지 검사하지 못했다면 while문의 처음으로 돌아가서 다시 랜덤 알파벳 설정
				else {
					user1[i] = r;
					break;  //중복이 없을 경우 user1 배열에 포함시킴. break문을 이용해 while문을 빠져나옴
				}

			}
		}

		for (k = 0; k < 25; k++) {  //user2 난수 만들기. user1 코드와 동일 (k : i 역할, l : j 역할)
			while (l<25)
			{
				r2 = rand() % 25 + 'A';

				for (l = 0; l <= k; l++)
					if (r2 == user2[l])
						break;
				if (l < k)
					continue;
				else {
					user2[k] = r2;
					break;
				}

			}
		}

		print_bingo(user1, user2, user3); //빙고판 출력 
	}
	else if (p == 3) { //입력된 값이 3일 때

		printf("User 1의 이름을 입력하세요.>> ");
		scanf("%s", name1);
		printf("User 2의 이름을 입력하세요.>> ");
		scanf("%s", name2);
		printf("User 3의 이름을 입력하세요.>> ");
		scanf("%s", name3);
		system("cls"); //화면 초기화

		for (i = 0; i < 25; i++) { //user1 난수 만들기 (입력값이 2였을 때의 코드와 같음)
			while (j<25)
			{
				r = rand() % 25 + 'A';

				for (j = 0; j <= i; j++)
					if (r == user1[j])
						break;
				if (j < i)
					continue;
				else {
					user1[i] = r;
					break;
				}

			}
		}

		for (k = 0; k < 25; k++) {  //user2 난수 만들기. user1 코드와 동일 (k : i 역할, l : j 역할)
			while (l<25)
			{
				r2 = rand() % 25 + 'A';

				for (l = 0; l <= k; l++)
					if (r2 == user2[l])
						break;
				if (l < k)
					continue;
				else {
					user2[k] = r2;
					break;
				}

			}
		}

		for (m = 0; m < 25; m++) {  //user3 난수 만들기. user1 코드와 동일 (m : i 역할, n : j 역할)
			while (n<25)
			{
				r3 = rand() % 25 + 'A';

				for (n = 0; n <= m; n++)
					if (r3 == user3[n])
						break;
				if (n < m)
					continue;
				else {
					user3[m] = r3;
					break;
				}

			}
		}

		print_bingo(user1, user2, user3); //빙고판 출력
	}
	printf("%s >>알파벳을 입력해주세요:", name1);
	scanf(" %c", &select1);	//알파벳 입력
	for (x = 0; x < 25; x++) { 
		if (user1[x] == select1) { //입력한 알파벳과 일치하는 배열 위치를 찾음
			user1[x] = '#';  //찾았을 시, 해당 값을 #으로 변경
			system("cls"); //화면 초기화
			print_bingo(user1, user2, user3); //빙고판 출력
			break;
		}
	}
	printf("%s >>알파벳을 입력해주세요:", name2);
	scanf(" %c", &select2);
	for (x = 0; x < 25; x++) {
		if (user2[x] == select2) {
			user2[x] = '#';
			system("cls");
			print_bingo(user1, user2, user3);
			break;
		}
	}
	if (p == 3) {
		printf("%s >>알파벳을 입력해주세요:", name3);
		scanf(" %c", &select3);
		for (x = 0; x < 25; x++) {
			if (user3[x] == select3) {
				user3[x] = '#';
				system("cls");
				print_bingo(user1, user2, user3);
				break;
			}
		}
	}

	free(user1);
	free(user2);
	free(user3); //각 동적메모리 해지
}

void print_bingo(char arr[25], char arr2[25], char arr3[25])
{
	a = 0; //a값 초기화
	b = 0; //b값 초기화

	if (p == 2) {
		printf("     [ %s ]               [ %s ]\n", name1, name2); //빙고판 위 사용자 이름 출력
		for (i = 0; i < 25; i++) {  //출력
			printf(" %c  ", arr[i]);
			if (i % 5 == 4) {  //한 줄에 user1[4]까지 출력 후 그 다음으로 user2[4]까지 출력시키는 방식으로 가로로 5x5 배열을 만들려고 함
				printf("  |  ");
				while (a <= i) {  //i가 출력한 범위까지 a도 출력하고자 함
					printf(" %c  ", arr2[a]);
					a++;
				}
				printf("\n");
			}
		}
	}
	else if (p == 3) {
		printf("     [ %s ]               [ %s ]               [ %s ]\n", name1, name2, name3); //빙고판 상단의 사용자 이름 출력
		for (i = 0; i < 25; i++) {  //출력
			printf(" %c  ", arr[i]);
			if (i % 5 == 4) {  //한 줄에 user1[4]까지 출력 후 그 다음으로 user2[4]까지 출력시키는 방식으로 가로로 5x5 배열을 만들려고 함
				printf("  |  ");
				while (a <= i) {  //i가 출력한 범위까지 a도 출력하고자 함
					printf(" %c  ", arr2[a]);
					a++;
				}
				printf("  |  ");
				while (b <= i) { //user3 배열 또한 i가 출력한 범위까지 출력
					printf(" %c  ", arr3[b]);
					b++;
				}
				printf("\n");
			}
		}

	}
}